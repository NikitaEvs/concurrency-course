#include <exe/executors/manual.hpp>

#include <fmt/core.h>

using namespace exe;

struct HelloTask : executors::TaskBase {
  void Run() noexcept override {
    fmt::println("Hello, world!");
  }
};

int main() {
  // No dynamic memory allocations here!

  executors::ManualExecutor manual;

  HelloTask hello;
  manual.Submit(&hello);
  manual.Drain();

  return 0;
}
