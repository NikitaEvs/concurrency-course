#pragma once

#include "atomic_ops.hpp"

//////////////////////////////////////////////////////////////////////

enum class MemoryOrder {
  Relaxed,
  Release,
  Acquire,
  SeqCst
};

//////////////////////////////////////////////////////////////////////

class Atomic {
 public:
  using Value = AtomicValue;

  explicit Atomic(Value init = 0)
      : storage_(init) {
  }

  // Non-copyable
  Atomic(const Atomic&) = delete;
  Atomic& operator=(const Atomic&) = delete;

  // Non-movable
  Atomic(Atomic&&) = delete;
  Atomic& operator=(Atomic&&) = delete;

  // Operations

  Value Load(MemoryOrder mo = MemoryOrder::SeqCst);

  operator Value() {
    return Load();
  }

  void Store(Value value, MemoryOrder mo = MemoryOrder::SeqCst);

  Atomic& operator=(Value value) {
    Store(value);
    return *this;
  }

  Value Exchange(Value new_value, MemoryOrder mo = MemoryOrder::SeqCst);

 private:
  AtomicStorage storage_;
};
