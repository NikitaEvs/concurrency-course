#include "../coroutine.hpp"

#include <wheels/test/framework.hpp>

#include <memory>
#include <optional>
#include <string>
#include <sstream>
#include <thread>

//////////////////////////////////////////////////////////////////////

struct TreeNode;

using TreeNodePtr = std::shared_ptr<TreeNode>;

struct TreeNode {
  TreeNodePtr left;
  TreeNodePtr right;
  std::string data;

  TreeNode(std::string d, TreeNodePtr l, TreeNodePtr r)
      : left(std::move(l)),
        right(std::move(r)),
        data(std::move(d)) {
  }

  static TreeNodePtr Branch(std::string data, TreeNodePtr left, TreeNodePtr right) {
    return std::make_shared<TreeNode>(
        std::move(data),
        std::move(left),
        std::move(right));
  }

  static TreeNodePtr Leaf(std::string data) {
    return std::make_shared<TreeNode>(
        std::move(data), nullptr, nullptr);
  }
};

//////////////////////////////////////////////////////////////////////

class TreeIterator {
  using SuspendContext = Coroutine::SuspendContext;

 public:
  explicit TreeIterator(TreeNodePtr root)
      : walker_([this, root](SuspendContext self) {
          TreeWalk(root, self);
        }) {
  }

  std::optional<std::string_view> Next() {
    if (walker_.IsCompleted()) {
      return std::nullopt;
    }
    
    next_.reset();
    walker_.Resume();
    return next_;
  }

 private:
  void TreeWalk(SuspendContext self, TreeNodePtr node) {
    if (node->left) {
      TreeWalk(self, node->left);
    }

    next_ = node->data;
    self.Suspend();

    if (node->right) {
      TreeWalk(self, node->right);
    }
  }

 private:
  Coroutine walker_;
  std::optional<std::string_view> next_;
};

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Coroutine) {
  SIMPLE_TEST(JustWorks) {
    Coroutine coro([](auto self) {
      self.Suspend();
    });

    ASSERT_FALSE(coro.IsCompleted());
    coro.Resume();
    ASSERT_FALSE(coro.IsCompleted());
    coro.Resume();
    ASSERT_TRUE(coro.IsCompleted());
  }

  SIMPLE_TEST(Interleaving) {
    int step = 0;

    Coroutine a([&](auto self) {
      ASSERT_EQ(step, 0);
      step = 1;
      self.Suspend();
      ASSERT_EQ(step, 2);
      step = 3;
    });

    Coroutine b([&](auto self) {
      ASSERT_EQ(step, 1);
      step = 2;
      self.Suspend();
      ASSERT_EQ(step, 3);
      step = 4;
    });

    a.Resume();
    b.Resume();

    ASSERT_EQ(step, 2);

    a.Resume();
    b.Resume();

    ASSERT_TRUE(a.IsCompleted());
    ASSERT_TRUE(b.IsCompleted());

    ASSERT_EQ(step, 4);
  }

  struct Threads {
    template <typename F>
    void Run(F task) {
      std::thread t([task = std::move(task)]() mutable {
        task();
      });
      t.join();
    }
  };

  SIMPLE_TEST(Threads) {
    size_t steps = 0;

    Coroutine coro([&steps](auto self) {
      std::cout << "Step" << std::endl;
      ++steps;
      self.Suspend();
      std::cout << "Step" << std::endl;
      ++steps;
      self.Suspend();
      std::cout << "Step" << std::endl;
      ++steps;
    });

    auto resume = [&coro] {
      coro.Resume();
    };

    // Simulate fiber running in thread pool
    Threads threads;
    threads.Run(resume);
    threads.Run(resume);
    threads.Run(resume);

    ASSERT_EQ(steps, 3);
  }

  SIMPLE_TEST(TreeWalk) {
    auto root = TreeNode::Branch(
        "B",
        TreeNode::Leaf("A"),
        TreeNode::Branch(
            "F",
            TreeNode::Branch(
                "D",
                TreeNode::Leaf("C"),
                TreeNode::Leaf("E")),
            TreeNode::Leaf("G")));

    std::stringstream traversal;

    TreeIterator iter(root);
    while (auto data = iter.Next()) {
      traversal << *data;
    }

    ASSERT_EQ(traversal.str(), "ABCDEFG");
  }

  SIMPLE_TEST(Pipeline) {
    const size_t kSteps = 123;

    size_t step_count = 0;

    Coroutine a([&](auto self) {
      Coroutine b([&](auto self) {
        for (size_t i = 0; i < kSteps; ++i) {
          ++step_count;
          self.Suspend();
        }
      });

      while (!b.IsCompleted()) {
        b.Resume();
        self.Suspend();
      }
    });

    while (!a.IsCompleted()) {
      a.Resume();
    }

    ASSERT_EQ(step_count, kSteps);
  }

  struct MyException {};

  SIMPLE_TEST(Exception) {
    Coroutine coro([&](auto self) {
      self.Suspend();
      throw MyException{};
      self.Suspend();
    });

    ASSERT_FALSE(coro.IsCompleted());
    coro.Resume();
    ASSERT_THROW(coro.Resume(), MyException);
    ASSERT_TRUE(coro.IsCompleted());
  }

  SIMPLE_TEST(NestedException1) {
    Coroutine a([](auto /*self*/) {
      Coroutine b([](auto /*self*/) {
        throw MyException();
      });
      ASSERT_THROW(b.Resume(), MyException);
    });

    a.Resume();
  }

  SIMPLE_TEST(NestedException2) {
    Coroutine a([](auto /*self*/) {
      Coroutine b([](auto /*self*/) {
        throw MyException();
      });
      b.Resume();
    });

    ASSERT_THROW(a.Resume(), MyException);

    ASSERT_TRUE(a.IsCompleted());
  }

  SIMPLE_TEST(ExceptionsHard) {
    int score = 0;

    Coroutine a([&](auto self) {
      Coroutine b([](auto /*self*/) {
        throw 1;
      });
      try {
        b.Resume();
      } catch (int) {
        ++score;
        // Context switch during stack unwinding
        self.Suspend();
        throw;
      }
    });

    a.Resume();

    std::thread t([&] {
      try {
        a.Resume();
      } catch (int) {
        ++score;
      }
    });
    t.join();

    ASSERT_EQ(score, 2);
  }

  SIMPLE_TEST(MemoryLeak) {
    auto shared_ptr = std::make_shared<int>(42);
    std::weak_ptr<int> weak_ptr = shared_ptr;

    {
      auto routine = [ptr = std::move(shared_ptr)](auto /*self*/) {};
      Coroutine coro(routine);
      coro.Resume();
    }

    ASSERT_FALSE(weak_ptr.lock());
  }
}

RUN_ALL_TESTS()
