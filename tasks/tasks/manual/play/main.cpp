#include <exe/executors/manual.hpp>

#include <exe/fibers/sched/go.hpp>
#include <exe/fibers/sched/yield.hpp>

#include <fmt/core.h>

using namespace exe;

int main() {
  executors::ManualExecutor scheduler;

  for (size_t i = 1; i <= 2; ++i) {
    fibers::Go(scheduler, [i] {
      for (size_t j = 1; j <= 3; ++j) {
        fmt::println("Fiber #{}: Step {}", i, j);
        fibers::Yield();
      }
    });
  }

  scheduler.RunAtMost(3);

  fmt::println("3 steps later");
  scheduler.Drain();

  return 0;
}
