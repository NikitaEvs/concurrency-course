#pragma once

#include <exe/executors/executor.hpp>

namespace exe::fibers {

using Scheduler = executors::IExecutor;

}  // namespace exe::fibers
