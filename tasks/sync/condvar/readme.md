# Condvar

## Пререквизиты

- [sync/mutex](/tasks/sync/mutex)

---

В этой задаче мы напишем [`std::condition_variable`](https://en.cppreference.com/w/cpp/thread/condition_variable).

## Кондвары

Условная переменная (_condition variable_) или просто _кондвар_ (_condvar_) – примитив синхронизации, который предоставляет потокам функциональность блокирующего ожидания событий. 

Под событием в контексте кондваров понимается модификация разделяемого состояния, защищенного мьютексом (например, очереди задач в пуле потоков).

## Операции

У кондвара есть один метод для ожидания (`Wait`) и два метода для уведомления (`NotifyOne` и `NotifyAll`).

Метод `Wait` можно вызывать только внутри критической секции, в него передается захваченный мьютекс.

Семантика `cv.Wait(mutex)` –

1. *Aтомарно* 
   a) отпустить `mutex` и 
   b) встать в очередь ожидания нотификации от `cv.NotifyOne()` или `cv.NotifyAll()`.
2. После пробуждения захватить обратно отпущенный `mutex` и завершить вызов.

До начала и после завершения вызова `cv.Wait(mutex)` поток владеет мьютексом.

Семантика `cv.NotifyOne()` / `cv.NotifyAll()` – разбудить один из потоков / все потоки, стоящие в очереди ожидания в вызове `Wait`.

Кондвар не запоминает нотификации: если в момент вызова `NotifyOne` или `NotifyAll` ни один поток не ждал внутри вызова `Wait`, то нотификация не оставит никаких следов.

### Futex

Можно сказать, что кондвар обобщает фьютекс: ждать можно на произвольном состоянии / предикате, а не только на одной ячейке памяти / зафиксированном условии ` == old`.

### Атомарность

Под атомарностью на шаге 1 метода `Wait` следует понимать атомарность _относительно_ `NotifyOne` / `NotifyAll`: 

Не допускается сценарий, когда поток (назовем его _consumer_) в методе `Wait` уже отпустил мьютекс, но еще не встал в очередь ожидания кондвара, и в этот момент другой поток (_producer_) захватил мьютекс, изменил разделяемое состояние и вызвал `cv.NotifyOne()`, но никого не разбудил.

### Spurious Wakeups

Метод `Wait` допускает *ложные пробуждения* (*spurious wakeups*):
- Вызов `cv.Wait(mutex)` может вернуть управление даже без нотификации от других потоков
- После вызова `NotifyOne` из вызова `Wait` могут выйти несколько потоков

Найдите сценарий (или сценарии) ложного пробуждения в своей реализации.

Ложные пробуждения можно назвать дефектом реализации, но исправлять их и думать о их обычно<sup>†</sup> нет необходимости: типичные паттерны корректного использования кондваров автоматически учитывают и ложные пробуждения.

<sup>†</sup> Пример исключения из этого правила вы встретите в одной из соседних задач.

## References

* [`std::condition_variable`](https://en.cppreference.com/w/cpp/thread/condition_variable)
* [`pthread_cond_timedwait`](https://man7.org/linux/man-pages/man3/pthread_cond_wait.3p.html)  
* [`tf::CondVar`](https://gitlab.com/Lipovsky/tinyfibers/-/blob/master/tf/sync/condvar.hpp) – реализация для однопоточных кооперативных файберов.

## Задание

Перенесите реализацию `Mutex` из задачи [sync/mutex](/tasks/sync/mutex) в [`mutex.hpp`](mutex.hpp), мьютекс и кондвар будут тестироваться вместе.

Реализуйте `CondVar` из [`condvar.hpp`](condvar.hpp).
 
## Реализация

### Futex

Как и мьютекс, кондвар должен блокировать и будить ждущие потоки. А значит для реализации нам потребуется уже знакомый инструмент – системный вызов [`futex`](https://man7.org/linux/man-pages/man2/futex.2.html), с которым мы работаем через [`Wait` из _Twist_](https://gitlab.com/Lipovsky/twist/-/blob/master/examples/wait/main.cpp).

### ABA

Скорее всего ваша реализация будет подвержена [_ABA problem_](https://en.wikipedia.org/wiki/ABA_problem) из-за переполнения 32-битного счетчика.

Посмотрите, как ABA пытаются решить в [`pthread_cond_wait`](https://github.com/lattera/glibc/blob/895ef79e04a953cac1493863bcae29ad85657ee1/nptl/pthread_cond_wait.c#L193).

Возможны ли ложные пробуждения в этой реализации?

### References

- [C] [`pthread_cond_wait` Implementation](https://github.com/bminor/glibc/blob/bb5bbc20702981c287aa3e44640e7d2f2b9a28cf/nptl/pthread_cond_wait.c#L192)
- [Rust] [Replace Linux Mutex and Condvar with futex based ones](https://github.com/rust-lang/rust/pull/95035)    