begin_task()

# Library
add_task_library(exe)

# Playground
add_task_playground(play)

# Tests

## Tasks

### Thread Pool
add_task_test(tp_unit_tests tests/executors/thread_pool/unit.cpp)
add_task_test(tp_stress_tests tests/executors/thread_pool/stress.cpp)

### Manual
add_task_test(manual_unit_tests tests/executors/manual/unit.cpp)

## Fibers

### Sched
add_task_test(fibers_sched_unit_tests tests/fibers/sched/unit.cpp)
add_task_test(fibers_sched_stress_tests tests/fibers/sched/stress.cpp)

end_task()
