#pragma once

#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////

typedef int64_t AtomicValue;
typedef volatile int64_t AtomicStorage;

////////////////////////////////////////////////////////////////////////////////

// Atomic operations

// Atomically loads and returns the current value of the atomic variable
AtomicValue AtomicLoad(AtomicStorage* loc);

// Atomically stores 'value' to memory location 'loc'
void AtomicStore(AtomicStorage* loc, AtomicValue value);

// Atomically replaces content of memory location `loc` with `value`,
// returns content of the location before the call
AtomicValue AtomicExchange(AtomicStorage* loc, AtomicValue value);
