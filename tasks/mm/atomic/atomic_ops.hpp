#pragma once

#include <cstdint>

////////////////////////////////////////////////////////////////////////////////

using AtomicValue = int64_t;
using AtomicStorage = volatile int64_t;

////////////////////////////////////////////////////////////////////////////////

// Atomic operations

// Load
// Atomically loads and returns the current value of the atomic variable

extern "C" AtomicValue AtomicLoadRelaxed(AtomicStorage* loc);
extern "C" AtomicValue AtomicLoadAcquire(AtomicStorage* loc);
extern "C" AtomicValue AtomicLoadSeqCst(AtomicStorage* loc);


// Store
// Atomically stores 'value' to memory location `loc`

extern "C" void AtomicStoreRelaxed(AtomicStorage* loc, AtomicValue value);
extern "C" void AtomicStoreRelease(AtomicStorage* loc, AtomicValue value);
extern "C" void AtomicStoreSeqCst(AtomicStorage* loc, AtomicValue value);

// Exchange
// Atomically replaces content of memory location `loc` with `value`,
// returns content of the location before the call

extern "C" AtomicValue AtomicExchangeSeqCst(AtomicStorage* loc, AtomicValue value);
