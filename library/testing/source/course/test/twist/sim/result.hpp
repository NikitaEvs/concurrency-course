#pragma once

#include <twist/sim.hpp>

#include <string>

namespace course::twist {
namespace sim {

std::string FormatFailure(::twist::sim::Result);

}  // namespace sim
}  // namespace course::twist
