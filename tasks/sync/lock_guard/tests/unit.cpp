#include <wheels/test/framework.hpp>

#include "../lock_guard.hpp"

#include <mutex>

TEST_SUITE(LockGuard) {
  SIMPLE_TEST(JustWorks) {
    std::mutex mutex;

    {
      LockGuard guard{mutex};

      ASSERT_FALSE(mutex.try_lock());
    }

    {
      mutex.lock();

      mutex.unlock();
    }
  }

  SIMPLE_TEST(Unwind) {
    std::mutex mutex;

    try {
      {
        LockGuard guard{mutex};

        // try_lock is allowed to fail spuriously:
        // https://en.cppreference.com/w/cpp/thread/mutex/try_lock
        for (size_t i = 0; i < 7; ++i) {
          ASSERT_FALSE(mutex.try_lock());
        }

        throw 1;
      }
    } catch (int) {
      // Ignore
    }

    {
      mutex.lock();
      mutex.unlock();
    }
  }

  SIMPLE_TEST(BasicLockable) {
    struct MutexStub {
      void lock() {
        // Nop
      }

      void unlock() {
        // Nop
      }
    };

    MutexStub mutex;

    {
      LockGuard guard{mutex};
    }
  }
}

RUN_ALL_TESTS()
