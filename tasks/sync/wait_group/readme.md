# WaitGroup

`WaitGroup` – счетчик абстрактной "работы" (например, запланированных в пул потоков задач).

Изначально в счетчике 0.

Операции:
- `void Add(size_t count)` – добавить положительный `count`
- `void Done()` – вычесть единицу
- `void Wait()` – заблокировать вызвавший метод поток до тех пор, пока значение счетчика не опустится до нуля

Будем считать, что `WaitGroup` – _одноразовый_ (_one-shot_), т.е. метод `Wait` можно вызвать только один раз.

Вызовы `Add` должны быть упорядочены (синхронизацией, формально – частичным порядком [_happens-before_](https://eel.is/c++draft/intro.races#def:happens_before)) с вызовом `Wait` **пользователем** `WaitGroup` .

Вызовы `Done` при этом могут (и будут) конкурировать с `Wait`.

## Пример

```cpp
void ThreadPoolExample() {
  ThreadPool pool{4};
  pool.Start();
  
  std::atomic<uint64_t> work{0};
  
  WaitGroup wg;
  
  for (size_t i = 0; i < 256; ++i) {
    wg.Add(1);
    
    pool.Submit([&] {
      work.fetch_add(1);
      wg.Done();
    });
  }
  
  // <- К этому моменту завершены все вызовы Add
  wg.Wait();  // Блокируем поток до выполнения всех задач в пуле
  
  fmt::println("Work = {}", work.load());
  
  pool.Stop();
}
```

## Go

Имя примитива взята из языка Go: [sync.WaitGroup](https://pkg.go.dev/sync#WaitGroup)

В программе на Go группы используются для организации иерархии горутин: [Go by Example: WaitGroups](https://gobyexample.com/waitgroups)

## Задание

Реализуйте [`WaitGroup`](wait_group.hpp)
