#include "random.hpp"

#include <course/test/time_budget.hpp>

#include <wheels/test/framework.hpp>

#include <fmt/core.h>

#if defined(__TWIST_SIM__)

// simulation

#include <twist/sim.hpp>

namespace course::twist {
namespace random {

size_t EstimateSize(TestBody body) {
  ::twist::sim::sched::FairScheduler scheduler;
  ::twist::sim::Simulator simulator{&scheduler};
  auto result = simulator.Run(body);
  return result.iters;
};

}  // namespace random
}  // namespace course::twist

#if defined(__TWIST_SIM_ISOLATION__)

// matrix

#include "sim/result.hpp"

namespace course::twist {
namespace random {

using ::twist::sim::sched::RandomScheduler;
using ::twist::sim::sched::PctScheduler;

struct Replay {
  ::twist::sim::sched::Schedule schedule;
  ::twist::sim::Digest digest;
};

Replay TryRecord(TestBody body, RandomScheduler& scheduler) {
  ::twist::sim::sched::Recorder recorder{&scheduler};
  ::twist::sim::Simulator simulator{&recorder};
  auto result = simulator.Run(body);
  return {recorder.GetSchedule(), result.digest};
}

void Check(TestBody body, Params) {
  if (!::twist::sim::DetCheck(body)) {
    FAIL_TEST("Test routine is not deterministic");
  }

  if (EstimateSize(body) > 1024) {
    FAIL_TEST("Simulation is too large for random checking");
  }

  course::test::TimeBudget time_budget;

  auto params = RandomScheduler::Params{};
  params.time_slice = 13;
  RandomScheduler scheduler{params};

  size_t sim_count = 0;

  do {
    ::twist::sim::Simulator simulator{&scheduler};
    auto result = simulator.Run(body);

    ++sim_count;

    if (result.Failure()) {
      static const size_t kNotTooLarge = 256;

      if (result.iters < kNotTooLarge) {
        auto replay = TryRecord(body, scheduler);
        if (replay.digest != result.digest) {
          WHEELS_PANIC("Twist error: failed to record simulation replay");
        }

        ::twist::sim::Print(body, replay.schedule);

        FAIL_TEST(sim::FormatFailure(result));
      }
    }
  } while (scheduler.NextSchedule() && time_budget);

  fmt::println("Executions checked: {}", sim_count);
}

}  // namespace random
}  // namespace course::twist

#else

// Simulation, maybe with sanitizers

namespace course::twist {
namespace random {

using ::twist::sim::sched::RandomScheduler;
using ::twist::sim::sched::PctScheduler;

void Check(TestBody body, Params) {
  // TODO: PctScheduler

  if (EstimateSize(body) > 1024) {
    FAIL_TEST("Simulation is too large for random checking");
  }

  auto params = RandomScheduler::Params{};
  params.time_slice = 13;
  RandomScheduler scheduler{params};

  size_t sim_count = 0;

  course::test::TimeBudget time_budget;

  do {
    ::twist::sim::Simulator simulator{&scheduler};
    auto result = simulator.Run(body);

    ++sim_count;

    if (result.Failure()) {
      // Impossible?
    }

  } while (scheduler.NextSchedule() && time_budget);

  fmt::println("Executions checked: {}", sim_count);
}

}  // namespace random
}  // namespace course::twist

#endif

#elif defined(__TWIST_FAULTY__)

// threads + fault injection

#include <twist/rt/thr/fault/adversary/adversary.hpp>

#include <twist/rt/thr/logging/logging.hpp>

#include <twist/test/new_iter.hpp>

namespace course::twist {
namespace random {

void TestIter(size_t iter, TestBody body, Params) {
  body();

  ::twist::rt::thr::log::FlushPendingLogMessages();

  ::twist::rt::thr::fault::Adversary()->Iter(iter);
}

void Check(TestBody body, Params params) {
  ::twist::rt::thr::fault::Adversary()->Reset();

  course::test::TimeBudget time_budget;

  size_t count = 0;

  while (time_budget) {
    TestIter(count, body, params);
    ++count;
  }

  fmt::println("Executions checked: {}", count);
  ::twist::rt::thr::fault::Adversary()->PrintReport();
}

}  // namespace random
}  // namespace course::twist

#else

// just threads

namespace course::twist {
namespace random {

void Check(TestBody body, Params) {
  WHEELS_PANIC("Random checking is not supported for this build");
}

}  // namespace random
}  // namespace course::twist

#endif
