#pragma once

#include "body.hpp"

#include <twist/build.hpp>

namespace course::twist {

namespace random {

constexpr bool Supported() {
  return ::twist::build::Faulty() || ::twist::build::Sim();
}

struct Params {
  std::chrono::milliseconds time_budget;
};

void Check(TestBody body, Params params);

}  // namespace random

}  // namespace course::twist
