#pragma once

#include <exe/queue.hpp>

// https://github.com/Naios/function2
#include <function2/function2.hpp>

#include <twist/ed/stdlike/thread.hpp>

// Fixed-size pool of worker threads

class ThreadPool {
 public:
  using Task = fu2::unique_function<void()>;

  explicit ThreadPool(size_t threads);
  ~ThreadPool();

  // Non-copyable
  ThreadPool(const ThreadPool&) = delete;
  ThreadPool& operator=(const ThreadPool&) = delete;

  // Non-movable
  ThreadPool(ThreadPool&&) = delete;
  ThreadPool& operator=(ThreadPool&&) = delete;

  // Launches worker threads
  void Start();

  // Schedules task for execution in one of the worker threads
  void Submit(Task);

  // Locates current thread pool from worker thread
  static ThreadPool* Current();

  // Stops the worker threads as soon as possible
  void Stop();

 private:
  // Worker threads, task queue, etc
};
